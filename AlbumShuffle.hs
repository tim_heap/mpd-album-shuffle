import qualified Network.MPD as MPD
import Data.Maybe (catMaybes, fromJust, fromMaybe)
import Network.MPD (sgFilePath, sgId, sgIndex, sgGetTag, sgId)
import Data.List (sortBy, groupBy, partition, elemIndex, minimumBy)
import Data.Function (on)
import qualified Data.ByteString
import System.Random.Shuffle (shuffleM)
import Data.Ord (comparing)
import Control.Arrow ((&&&))

getMeta :: MPD.Metadata -> String -> MPD.Song -> String
getMeta meta fallback song = if null tags then fallback else MPD.toString $ head tags
    where tags = fromMaybe [] $ sgGetTag meta song

getArtist = getMeta MPD.Artist ""
getAlbum = getMeta MPD.Album ""
getNumberMeta meta song
    | null answers = 0
    | otherwise = fst . head $ answers
    where answers = (reads $ getMeta meta "0" song) :: [(Integer, String)]
getTrack = getNumberMeta MPD.Track
getDisc = getNumberMeta MPD.Disc

thenBy :: (a -> a -> Ordering) -> (a -> a -> Ordering) -> a -> a -> Ordering
thenBy pri sec a b
    | ord == EQ = sec a b
    | otherwise = ord
    where ord = pri a b

-- TODO Work out what the hell irc.freenode.net#haskell is on about
-- thenBy = (liftA2.liftA2) mappend

sensibleAlbumSort :: MPD.Song -> MPD.Song -> Ordering
sensibleAlbumSort = comparing getAlbum `thenBy` comparing getDisc `thenBy` comparing getTrack

partitionOnCurrentAlbum :: Maybe MPD.Song -> [MPD.Song] -> ([MPD.Song], [MPD.Song])
partitionOnCurrentAlbum Nothing playlist = ([], playlist)
partitionOnCurrentAlbum (Just current) playlist = partition ((== currentAlbum) . getAlbum) playlist
    where currentAlbum = getAlbum current

groupByAlbum :: [MPD.Song] -> [[MPD.Song]]
groupByAlbum playlist = groupedByAlbum
    where
        sortedByAlbum = sortBy sensibleAlbumSort playlist
        groupedByAlbum = groupBy ((==) `on` getAlbum) sortedByAlbum


-- Gets the position in xs of elements in the second list (ys)
-- Assumes that ys is a permutation of xs, with identical elements in a
-- different order
indices :: Eq a => [a] -> [a] -> [(Int, Int)]
indices xs ys = zip (map (\x -> fromJust $ x `elemIndex` xs) ys) [0 ..]

getSwapsfromIndices :: [(Int, Int)] -> [(Int, Int)]
getSwapsfromIndices xs = getSwapsfromIndices' xs []

-- The second argument for this is an accumulator used for tail recursion
getSwapsfromIndices' :: [(Int, Int)] -> [(Int, Int)] -> [(Int, Int)]
getSwapsfromIndices' [] ys = ys
getSwapsfromIndices' xs ys = getSwapsfromIndices' xs' (ys ++ new_swap)
   where (l1, l2) = minimumBy (compare `on` snd) xs
         -- remove minimum from the list
         unordered = [ (x, y)  | (x, y) <- xs, y /= l2]
         -- swap
         xs' = [ (if  x == l2 then l1 else x, y)  | (x, y) <- unordered]
         -- if no swap is needed, do not append anything
         new_swap = if l1 == l2 then [] else [(l1, l2)]

swaps :: Eq a => [a] -> [a] -> [(Int, Int)]
swaps xs ys = getSwapsfromIndices $ indices xs ys

albumArtist :: [MPD.Song] -> String
albumArtist songs
    | sameArtist = head artists
    | otherwise = "V-A"
    where artists = map getArtist songs
          sameArtist = allEqual artists

allEqual :: Eq a => [a] -> Bool
allEqual [] = undefined
allEqual [x] = True
allEqual (x1:x2:xs) = (x1 == x2) && (allEqual (x2:xs))

showAlbum :: [MPD.Song] -> String
showAlbum songs = artist ++ " - " ++ album
    where artist = albumArtist songs
          album = getAlbum $ head songs

main = do
        playlist' <- MPD.withMPD $ MPD.playlistInfo Nothing
        currentSong' <- MPD.withMPD MPD.currentSong

        let playlist = either (const []) id playlist'
        let currentSong = either (const Nothing) id currentSong'

        let (currentAlbum', otherSongs) = partitionOnCurrentAlbum currentSong playlist
        let currentAlbum = sortBy sensibleAlbumSort currentAlbum'
        let otherAlbums = groupByAlbum otherSongs

        otherShuffledAlbums <- shuffleM otherAlbums
        let shuffledAlbums = currentAlbum : otherShuffledAlbums
        let shuffledSongs = concat shuffledAlbums

        let swapSongs (src, dest) = MPD.withMPD $ MPD.swap src dest

        mapM_ swapSongs (swaps playlist shuffledSongs)

        putStrLn "New playlist order:"
        mapM_  putStrLn $ map showAlbum shuffledAlbums

        return ()
