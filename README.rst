MPD album shuffle
=================

Shuffle an MPD playlist by album.

Compiling
---------

    cabal sandbox init
    cabal update
    cabal install --only-dependencies
    cabal build
